#ifndef PREFETCH_H
#define PREFETCH_H

#include <cstdint>
#include <iostream>


class System;

class Prefetch {
protected:
   const int line_size = 64;
public:
   virtual ~Prefetch() {}
   virtual int prefetchMiss(uint64_t address, unsigned int tid,
                              System* sys)=0;
   virtual int prefetchHit(uint64_t address, unsigned int tid,
                              System* sys)=0;
};

//"Prefetcher" that does nothing
class NullPrefetch : public Prefetch {
public:
   ~NullPrefetch() {}
   int prefetchMiss(uint64_t address, unsigned int tid,
                              System* sys);
   int prefetchHit(uint64_t address, unsigned int tid,
                              System* sys);
};

class AdjPrefetch : public Prefetch {
public:
   ~AdjPrefetch() {}
   int prefetchMiss(uint64_t address, unsigned int tid,
                              System* sys);
   int prefetchHit(uint64_t address, unsigned int tid,
                              System* sys);
};

class SeqPrefetch : public Prefetch {
private:
   size_t N;

public:
   SeqPrefetch(size_t n) { N = n;}
   ~SeqPrefetch() {}
   int prefetchMiss(uint64_t address, unsigned int tid,
                              System* sys);
   int prefetchHit(uint64_t address, unsigned int tid,
                              System* sys);
};

class BestEffortPrefetch : public Prefetch {
// private:
   //bool firstrun;
   //int64_t distance;
public:
   //BestEffortPrefetch(bool f, int64_t d) { firstrun = f; distance = d;}
   ~BestEffortPrefetch() {}
   int prefetchMiss(uint64_t address, unsigned int tid,
                              System* sys);
   int prefetchHit(uint64_t address, unsigned int tid,
                              System* sys);
};

#endif
