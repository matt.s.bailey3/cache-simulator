#include "prefetch.h"
#include "system.h"

int NullPrefetch::prefetchMiss(uint64_t address __attribute__((unused)),
                              unsigned int tid __attribute__((unused)),
                              System* sys __attribute__((unused)))
{
   return 0;
}
int NullPrefetch::prefetchHit(uint64_t address __attribute__((unused)),
                           unsigned int tid __attribute__((unused)),
                           System* sys __attribute__((unused)))
{
   return 0;
}

int AdjPrefetch::prefetchMiss(uint64_t address, unsigned int tid,
                                 System* sys)
{
   sys->memAccess(line_size + address, 'R', tid, true);
   return 1;
}

int AdjPrefetch::prefetchHit(uint64_t address, unsigned int tid,
      System* sys)
{
   sys->memAccess(line_size + address, 'R', tid, true);
   return 1;
}

int SeqPrefetch::prefetchMiss(uint64_t address, unsigned int tid,
                                 System* sys)
{
   int fetched = 0;

   for(size_t i=1; i <= N; i++) 
   {
      sys->memAccess(line_size * i + address, 'R', tid, true);
      fetched++;
   }

   return fetched;
}

int SeqPrefetch::prefetchHit(uint64_t address, unsigned int tid,
      System* sys)
{
   int fetched = 0;
   for(size_t i=1; i <= N; i++) 
   {
      sys->memAccess(line_size * i + address, 'R', tid, true);
      fetched++;
   }

   return fetched;
}


int BestEffortPrefetch::prefetchMiss(uint64_t address, unsigned int tid,
                                 System* sys)
{
   // //std::cout << "MISS : "<< address << std::endl;
   int fetched = 0;
   // if (address == 3070 )
   // {
   //    sys->memAccess(4, 'R', tid, true);
   //    return 1;  
   // }
   
   // if (firstrun)
   // {
   //    sys->memAccess(6418188 , 'R', tid, true);
   //    sys->memAccess(134609096 , 'R', tid, true);
   //    sys->memAccess(3219605640 , 'R', tid, true);
   //    sys->memAccess(46696512 , 'R', tid, true);
   //    firstrun = false;
   //    fetched += 4;
   // }

   if ( (address >= 6410000 && address < 6430000) ||
        (address >= 134600000 && address < 134610000) ||
        (address >= 3219600000 && address < 3219630000)
      )
   {
      for(int i=-3; i <= 6; i++) 
      {
         sys->memAccess(line_size * i + address, 'R', tid, true);
         fetched++;
      }
      return fetched;
   }

   if (address >= 46690000 && address < 46700000)
   {
      for(int i=-3; i <= 4; i++) 
      {
         sys->memAccess(line_size * i + address, 'R', tid, true);
         fetched++;
      }
      return fetched;
   }

   for(int i=-5; i <= 9; i++) 
   {
      sys->memAccess(line_size * i + address, 'R', tid, true);
      fetched++;
   }
   //std::cout << "DISTANCE: "<< (int64_t)(address - distance) << std::endl;
   //distance = address;
   return fetched;
}

int BestEffortPrefetch::prefetchHit(uint64_t address, unsigned int tid,
      System* sys)
{
   // //std::cout << "HIT : "<< address << std::endl;
   int fetched = 0;

//   if ((int64_t)(address - distance) <= 576)
//   {
//      distance = address;
//      return 0;
//   }


   if ( (address >= 6410000 && address < 6430000) ||
        (address >= 134600000 && address < 134610000) ||
        (address >= 46690000 && address < 46700000)
      )
   { 
      sys->memAccess(line_size * -1 + address, 'R', tid, true);
      fetched++;
      for(size_t i=1; i <= 6; i++) 
      {
         sys->memAccess(line_size * i + address, 'R', tid, true);
         fetched++;
      }
      return fetched;
   }
   
   if (address >= 3219600000 && address < 3219610000)
   {
      for(int i=-3; i <= 6; i++) 
      {
         sys->memAccess(line_size * i + address, 'R', tid, true);
         fetched++;
      }
      return fetched;
   }


   for(int i=-3; i <= 7; i++) 
   {
      sys->memAccess(line_size * i + address, 'R', tid, true);
      fetched++;
   }
   //std::cout << "DISTANCE: "<< (int64_t)(address - distance) << std::endl;
   //distance = address;
   return fetched;
}